package com.company.LibraryManagementSystem;

import java.util.Scanner;

public class LibraryManagementSystem {

    private Library library;

    LibraryManagementSystem(String x){
        library.setNameOfLibrary(x);
    }

    LibraryManagementSystem(){
        library.setNameOfLibrary("Generic");
    }

    public void RunLibraryMS(){
        System.out.println("Welcome to the " + library.getNameOfLibrary() + " library");
        library.setArrayOfBooks(new Book[]{
                new Book("aaa1", "bbb1", "ccc1", 100, "eee1", "fff1", 100d),
                new Book("aaa2", "bbb2", "ccc2", 150, "eee2", "fff2", 200d),
                new Book("aaa3", "bbb3", "ccc3", 200, "eee3", "fff3", 300d)
        });
        Scanner scan = new Scanner(System.in);
        int selector = 0;

        do{
            System.out.println( "1.Show books.\n" +
                                "2.Buy a book. (Offer possibility to choose a book)\n" +
                                "3.Show books by name enter from the console. (Offer possibility to choose a book)\n" +
                                "4.Show books by authorNme enter from the console.(Offer possibility to choose a book)\n" +
                                "5.Show books by editorialName enter from the console.(Offer possibility to choose a book)\n" +
                                "6.Show books by price enter from the console.(Offer possibility to choose a book)\n" +
                                "7.Show books that have more then nrOfPages enter from the console.(Offer possibility to choose a book)\n" +
                                "8.Show books by kindOfLiterature enter from the console.(Offer possibility to choose a book)\n" +
                                "9.Calculate and show total price for all books in library.\n" +
                                "10.Calculate and show the avarage price of all books.\n" +
                                "11.Show the book with the lowest price.\n" +
                                "12.Show the book with the highest price.  \n" +
                                "13.Show the book with the largest number of pages.\n" +
                                "14.Show the book with the smallest number of pages.\n" +
                                "15.Show the book with the longest name.\n" +
                                "16.Exit\n");

            selector = scan.nextInt();

            switch (selector) {
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 5:
                    break;
                case 6:
                    break;
                case 7:
                    break;
                case 8:
                    break;
                case 9:
                    break;
                case 10:
                    break;
                case 11:
                    break;
                case 12:
                    break;
                case 13:
                    break;
                case 14:
                    break;
                case 15:
                    break;
                default:
                    break;
            }
        }while(selector != 16);


    }

}
