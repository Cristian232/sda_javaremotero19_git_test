package com.company.LibraryManagementSystem;

public class Library {
    private Book[] arrayOfBooks;
    private String nameOfLibrary;

    public void setArrayOfBooks(Book[] arrayOfBooks) {
        this.arrayOfBooks = arrayOfBooks;
    }

    public void setNameOfLibrary(String nameOfLibrary) {
        this.nameOfLibrary = nameOfLibrary;
    }

    public Book[] getArrayOfBooks() {
        return arrayOfBooks;
    }

    public String getNameOfLibrary() {
        return nameOfLibrary;
    }


}
