package com.company.LibraryManagementSystem;

public class Book {
    private String name;
    private String isbn;
    private String authorName;
    private int nrOfPages;
    private String editorialName;
    private String kindOfLiterature;
    private double price;

    public String getName() {
        return name;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getAuthorName() {
        return authorName;
    }

    public int getNrOfPages() {
        return nrOfPages;
    }

    public String getEditorialName() {
        return editorialName;
    }

    public String getKindOfLiterature() {
        return kindOfLiterature;
    }

    public Double getPrice() {
        return price;
    }

    public Book(String name, String isbn, String authorName, int nrOfPages, String editorialName, String kindOfLiterature, double price) {
        this.name = name;
        this.isbn = isbn;
        this.authorName = authorName;
        this.nrOfPages = nrOfPages;
        this.editorialName = editorialName;
        this.kindOfLiterature = kindOfLiterature;
        this.price = price;
    }
}
